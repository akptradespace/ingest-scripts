#/bin/bash

directory=https://publication.epo.org/raw-data/product?productId=23

wget --load-cookies cookies.txt $directory

grep zip product\?productId\=23  | grep download/ |  grep docdb | awk -F"'" '{ print $2 }' |   awk 'NF' | awk '{ print "https://publication.epo.org/raw-data/" $1 }' > file-list.txt

rm product\?productId\=23
