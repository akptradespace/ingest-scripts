#/bin/bash

filelist='file-list.txt'
dest_dir='docdb-files'

echo "Downloading files specified in $filelist"

while read file; do 
    echo 'downloading file ' $file
    filename=`echo $file | cut -d'/' -f11`
    echo 'filename is ' $filename
    wget --load-cookies cookies.txt $file wget --output-document $dest_dir/$filename
done < $filelist
